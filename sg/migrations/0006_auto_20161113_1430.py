# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sg', '0005_auto_20161113_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='telegram_name',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0418\u043c\u044f \u0432 Telegram', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='telegram_chat_id',
            field=models.CharField(default=b'', max_length=255, verbose_name='\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0447\u0430\u0442\u0430 \u0432 Telegram'),
        ),
    ]
