# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sg', '0003_telegram'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telegram',
            name='chat_id',
            field=models.CharField(max_length=255),
        ),
    ]
