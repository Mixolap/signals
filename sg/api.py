#-*- encoding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.db import connection
import random
import datetime
import time
import json
import requests
import urllib
from models import *



def apiSend(request):
    guid     = request.GET.get("guid")
    mfrom    = request.GET.get("from","")
    priority = int(request.GET.get("priority",0))
    text     = request.GET.get("text","")[:2048]
        
        
    if guid==None: return HttpResponse("1")
    up = UserProfile.objects.filter(guid=guid)
    if up.count()==0: return HttpResponse("2")
    user = up[0].user
    
    Message(user=user,guid=guid,mfrom=mfrom,priority=priority,text=text).save()
    MessageHistory(user=user,guid=guid,mfrom=mfrom,priority=priority,text=text,telegram=(up[0].telegram_chat_id!='')).save()
    ms = MessageLast.objects.filter(user=user,mfrom=mfrom)
    if ms.count()==0:
        MessageLast(user=user,mfrom=mfrom,text=text,priority=priority).save()
    else:
        m = ms[0]
        m.text = text
        m.priority = priority
        m.save()

    return HttpResponse("accepted")
    
def apiLogin(request):
    username = request.GET.get("u","")
    password = request.GET.get("p","")
    #print username,password
    luser = authenticate(username=username, password=password)
    #print username,password,luser
    if luser==None: 
        return HttpResponse("no")
    
    return HttpResponse("yes")
    
def apiFroms(request):

    username = request.GET.get("u","")
    password = request.GET.get("p","")

    user = authenticate(username=username, password=password)
    if user==None: 
        return HttpResponse(json.dumps({"error":"no such user"}))
    
    data = {"data":[]}
    for d in MessageLast.objects.filter(user=user).order_by("-dt"):
        data["data"].append({
            'from':d.mfrom,
            'lastm':d.text,
            'dt':d.dt.strftime("%d.%m.%y %H:%M"),
            'priority':d.priority,
            })
    
    return HttpResponse(json.dumps(data))


def apiMessages(request):
    username = request.GET.get("u","")
    password = request.GET.get("p","")
    mfrom    = request.GET.get("from","")
    
    user = authenticate(username=username, password=password)
    if user==None: 
        return HttpResponse(json.dumps({"error":"no such user"}))
    
    data = {"data":[]}
    for d in MessageHistory.objects.filter(user=user,mfrom=mfrom).order_by("-id"):
        data["data"].append({
            'text':d.text,
            'dt':d.dt.strftime("%d.%m.%y %H:%M"),
            'priority':d.priority,
            })    
    
    return HttpResponse(json.dumps(data))
    
    
    
    