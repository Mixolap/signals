#-*- encoding: utf-8 -*-
import sys
import requests

API_URL = "http://api.mysignals.ru/api/backup/"

def send_backup(fname,mfrom='mysignals.ru',guid='79ffj7q63v39btb85c5h'):
    name = fname.split("/")[-1]
    r = requests.post(API_URL, files={name: open(fname, 'rb')})
    return r.content
    
if __name__=="__main__":
    print send_backup(sys.argv[1])