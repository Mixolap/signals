#-*- encoding: utf-8 -*-
import os
import django
import urllib
import requests
import json

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "signals.settings")
django.setup()

from sg.models import *

BOT_ID='278401067:AAGwBJRHu9M551PYI8JxvekSi33OvZTYGMk'


def sendTelegramMessage(chat_id,text):
    s = urllib.urlencode({
        'chat_id':chat_id,
        'text':unicode(text).encode('utf-8'),
    })
    requests.get("http://api.telegram.org/bot%s/sendMessage?%s"%(BOT_ID,s))
    

def sendMessages():
    for m in MessageHistory.objects.filter(telegram=True,telegram_sended=False).order_by("id"):        
        up = UserProfile.objects.get(user=m.user)
        if up.telegram_chat_id=="": continue
        sendTelegramMessage(up.telegram_chat_id,m.fullText())
        m.telegram_sended=True
        m.save()

def checkUpdates():
    r = requests.get("http://api.telegram.org/bot%s/getUpdates"%BOT_ID)
    for d in json.loads(r.content)['result']:
        username = d['message']['chat']['username']
        chat_id  = d['message']['chat']['id']
        Telegram.objects.update_or_create(username=username,defaults={'chat_id':chat_id})
        
    for d in UserProfile.objects.filter(telegram_chat_id='').exclude(telegram_name=None):
        tm = Telegram.objects.filter(username=d.telegram_name)
        if not tm.exists(): continue
        d.telegram_chat_id = tm[0].chat_id
        d.save()
        sendTelegramMessage(d.telegram_chat_id,u"Вы опознаны системой. Добро пожаловать.")

if __name__=="__main__":
    sendMessages()
    checkUpdates()
    






